package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	public void register(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", user_name");
			sql.append(", branch_id");
			sql.append(", section_id");
			sql.append(", is_stopped");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // login_id
			sql.append(", ?"); // password
			sql.append(", ?"); // user_name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // section_id
			sql.append(", 0"); // is_stopped
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getUser_name());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getSection_id());
//			ps.setString(6, user.getIs_stopped());
			System.out.println(ps);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String login_id,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> userList = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String user_name = rs.getString("user_name");
				int branch_id = rs.getInt("branch_id");
				int section_id = rs.getInt("section_id");
				int is_stopped = rs.getInt("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setPassword(password);
				user.setUser_name(user_name);
				user.setBranch_id(branch_id);
				user.setSection_id(section_id);
				user.setIs_stopped(is_stopped);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				userList.add(user);
			}
			return userList;
		} finally {
			close(rs);
		}
	}

	public List<User> getAllUsers(Connection connection) {

//		List<User> userDatas = new ArrayList<User>();
		ResultSet rs = null;


		PreparedStatement ps = null;
		try {
			 String sql = "SELECT * FROM users ORDER BY branch_id asc , section_id asc";

			ps = connection.prepareStatement(sql);

			rs = ps.executeQuery();

			List<User> userList = toUserList(rs);
			return userList;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void updateUserState(Connection connection , User user) {


		PreparedStatement ps = null;
		try {
			String sql = "UPDATE users SET is_stopped = ? WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, user.getIs_stopped());
			ps.setInt(2, user.getId());
			System.out.println(ps);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void updateEditedUser(Connection connection , User user) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET login_id = ? , user_name = ? , branch_id = ? , section_id = ?");

			String encPassword = user.getPassword();
			if(!StringUtils.isEmpty(encPassword)){
				sql.append(" , password = ?");
			}
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getUser_name());
			ps.setInt(3, user.getBranch_id());
			ps.setInt(4, user.getSection_id());

			if(!StringUtils.isEmpty(encPassword)){
				ps.setString(5, encPassword);
				ps.setInt(6, user.getId());
			} else {
				ps.setInt(5, user.getId());
			}

//			System.out.println(ps);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User checkNewUserId(Connection connection , String login_id){

    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<User> userList = new ArrayList<User>();

    	try{
    		String sql = "SELECT * FROM users WHERE login_id = ? ";

    		ps = connection.prepareStatement(sql);
    		ps.setString(1, login_id);

//    		System.out.println("ps：" + ps);


    		rs = ps.executeQuery();
    		userList = toUserList(rs);

    		if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}


    	} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);

		}
	}
}
