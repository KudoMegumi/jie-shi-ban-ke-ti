package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Section;
import exception.SQLRuntimeException;

public class SectionDao {
	public List<Section> getAllSections(Connection connection) {

		ResultSet rs = null;

		PreparedStatement ps = null;
		try {
			 String sql = "SELECT * FROM sections";

			ps = connection.prepareStatement(sql);

			rs = ps.executeQuery();

			List<Section> sectionList = toSectionList(rs);
			return sectionList;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Section> toSectionList(ResultSet rs) throws SQLException {

		List<Section> sectionList = new ArrayList<Section>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Section section = new Section();
				section.setId(id);
				section.setName(name);

				sectionList.add(section);
			}
			return sectionList;
		} finally {
			close(rs);
		}
	}

	public List<Section> getSectionData(Connection connection) {

		List<Section> sections = new ArrayList<Section>();
		ResultSet rs = null;

		String sql = "SELECT * FROM sections";
		PreparedStatement ps = null;
	   		try{
	   			ps = connection.prepareStatement(sql);
	   			rs = ps.executeQuery();
	   			while(rs.next()){

	   				Section sectionData = new Section();
	   				sectionData.setId(rs.getInt("id"));
	   				sectionData.setName(rs.getString("name"));


					sections.add(sectionData);
	    		}
	    		return sections;

	   		} catch (SQLException e) {

	   			throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	            close(rs);
	            }
	    	}
}
