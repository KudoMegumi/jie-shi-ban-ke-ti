package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {
	public void insert(Connection connection, Comment comment) {

    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");

            sql.append("contribution_id");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(") VALUES (");
            sql.append("?"); //contribution_id
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?"); // user_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getContribution_id());
            ps.setString(2, comment.getText());
            ps.setInt(3, comment.getUser_id());
            System.out.println(ps);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public List<Comment> getComment(Connection connection){

		List<Comment> comments = new ArrayList<Comment>();

		ResultSet rs = null;

		String sql = "select * from comments INNER JOIN users ON comments.user_id = users.id ORDER BY comments.created_date asc";
		PreparedStatement ps = null;
		try{
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){

				Comment comment = new Comment();
				comment.setUser_id(rs.getInt("comments.user_id"));
				comment.setId(rs.getInt("comments.id"));
				comment.setContribution_id(rs.getInt("comments.contribution_id"));
				comment.setText(rs.getString("comments.text"));
				comment.setCreated_date(rs.getTimestamp("comments.created_date"));
				comment.setName(rs.getString("users.user_name"));

				comments.add(comment);
			}
			return comments;

		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
            close(rs);
        }
	}

	public void deleteComment(Connection connection , int commentId) {

		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM comments WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, commentId);
			System.out.println(ps);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
