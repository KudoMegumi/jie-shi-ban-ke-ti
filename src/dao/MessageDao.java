package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class MessageDao {

	public List<UserMessage> getUserMessage(Connection connection,String search_word, String start_date, String end_date){

		List<UserMessage> messages = new ArrayList<UserMessage>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM contributions INNER JOIN users ON contributions.user_id = users.id ");
        sql.append("WHERE contributions.created_date BETWEEN ? AND ? ");

        if(!StringUtils.isEmpty(search_word)){
        	sql.append("AND category LIKE ? ");
        }
        sql.append("ORDER BY contributions.created_date desc");

		if(StringUtils.isEmpty(start_date)) {
			start_date = "2000-01-01 00:00:00";
		}else {
			start_date = start_date + " 00:00:00";
		}
		if(StringUtils.isEmpty(end_date)) {
			end_date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		}else {
			end_date = end_date + " 23:59:59";
		}

//		System.out.println("s：" + start_date);
//		System.out.println("e：" + end_date);

		try{

			Timestamp s_date = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(start_date).getTime());
			Timestamp e_date = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(end_date).getTime());
			ps = connection.prepareStatement(sql.toString());

			ps.setTimestamp(1,s_date);
			ps.setTimestamp(2,e_date);

			if(!StringUtils.isEmpty(search_word)){
				ps.setString(3,"%" + search_word +"%");
	        }
//			System.out.println("ps：" + ps);

			rs = ps.executeQuery();
			while(rs.next()){

				UserMessage message = new UserMessage();
				message.setId(rs.getInt("contributions.id"));
				message.setTitle(rs.getString("contributions.title"));
				message.setText(rs.getString("contributions.text"));
				message.setCategory(rs.getString("contributions.category"));
				message.setUser_id(rs.getInt("contributions.user_id"));
				message.setCreated_date(rs.getTimestamp("contributions.created_date"));
				message.setName(rs.getString("users.user_name"));

				messages.add(message);

			}
//			System.out.println("messages：" + messages);
			return messages;

		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } catch ( ParseException e) {
            e.printStackTrace();
        }
		finally {
            close(ps);
            close(rs);
        }
		return messages;
	}

    public void insert(Connection connection, UserMessage message) {

    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO contributions ( ");

            sql.append("title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(") VALUES (");
            sql.append("?"); // title
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?"); // user_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getTitle());
            ps.setString(2, message.getText());
            ps.setString(3, message.getCategory());
            ps.setInt(4, message.getUser_id());
//            System.out.println(ps);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void deleteMessage(Connection connection , int contributionId) {

		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM contributions WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, contributionId);
//			System.out.println(ps);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}