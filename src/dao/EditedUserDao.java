package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class EditedUserDao {

	public User getEditedUser(Connection connection,int id) {

		ResultSet rs = null;

        String sql = "SELECT * FROM users WHERE id = ? ";
		PreparedStatement ps = null;

		List<User> editedUserDataList = new ArrayList<User>();
    		try{
    			ps = connection.prepareStatement(sql);
    			ps.setInt(1, id);
    			rs = ps.executeQuery();

    			while (rs.next()) {
    				User editedUserData = new User();
    				editedUserData.setId(rs.getInt("id"));
    				editedUserData.setLogin_id(rs.getString("login_id"));
					editedUserData.setUser_name(rs.getString("user_name"));
					editedUserData.setBranch_id(rs.getInt("branch_id"));
					editedUserData.setSection_id(rs.getInt("section_id"));
					editedUserData.setIs_stopped(rs.getInt("is_stopped"));

					editedUserDataList.add(editedUserData);
    			}

    			if (editedUserDataList.isEmpty() == true) {
    				return null;
    			} else if (2 <= editedUserDataList.size()) {
    				throw new IllegalStateException("2 <= userList.size()");
    			} else {
    				return editedUserDataList.get(0);
    			}
    		} catch (SQLException e) {
    			throw new SQLRuntimeException(e);
    		} finally {
    			close(ps);
    	}
	}
}


