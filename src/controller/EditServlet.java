package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Section;
import beans.User;
import service.BranchService;
import service.EditService;
import service.SectionService;
import service.UserService;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet(urlPatterns = { "/edit"})
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		String str_id = request.getParameter("id");
//System.out.println(str_id);

		if (StringUtils.isEmpty(str_id) || !(str_id.matches("^[0-9]+$"))) {
			messages.add("不正なパラメータです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}

		if (messages.size() != 0) {
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("management");
			return;
		}

		int id = Integer.parseInt(request.getParameter("id"));
//		System.out.println("id:" + id);
		User editedUserData = new User();
		EditService editService = new EditService();


		List<Branch> branchList = new ArrayList<Branch>();
		BranchService branchService = new BranchService();

		List<Section> sectionList = new ArrayList<Section>();
		SectionService sectionService = new SectionService();

		editedUserData = editService.getEditedUser(id);
//		System.out.println(editedUserData);

		if (editedUserData == null) {
			messages.add("不正なパラメータです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}

		branchList = branchService.getAllBranches();
		sectionList = sectionService.getAllSections();

		request.setAttribute("editedUserData", editedUserData);
		session.setAttribute("branchList", branchList);
		session.setAttribute("sectionList", sectionList);
		request.getRequestDispatcher("edit.jsp").forward(request, response);

	}


    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

    	 List<String> messages = new ArrayList<String>();
    	 HttpSession session = request.getSession();
    	 if (isValid(request, messages) == true) {

	    	User user = new User();
	    	int id = Integer.parseInt(request.getParameter("editedUserData_id"));
	    	String login_id = request.getParameter("login_id");
	    	String password = request.getParameter("password");
	    	String user_name = request.getParameter("user_name");
	    	int branch_id = Integer.parseInt(request.getParameter("branch_id"));
	    	int section_id = Integer.parseInt(request.getParameter("section_id"));

	    	user.setId(id);
	    	user.setLogin_id(login_id);
	    	user.setPassword(password);
	    	user.setUser_name(user_name);
	    	user.setBranch_id(branch_id);
	    	user.setSection_id(section_id);

	    	EditService editService = new EditService();
	    	editService.updateUserData(user);

	        response.sendRedirect("management");

    	} else {
            session.setAttribute("errorMessages", messages);

            User editedUserData = new User();
            int id = Integer.parseInt(request.getParameter("editedUserData_id"));
	    	String login_id = request.getParameter("login_id");
	    	String password = request.getParameter("password");
	    	String user_name = request.getParameter("user_name");
	    	int branch_id = Integer.parseInt(request.getParameter("branch_id"));
	    	int section_id = Integer.parseInt(request.getParameter("section_id"));

	    	editedUserData.setId(id);
	    	editedUserData.setLogin_id(login_id);
	    	editedUserData.setPassword(password);
	    	editedUserData.setUser_name(user_name);
	    	editedUserData.setBranch_id(branch_id);
	    	editedUserData.setSection_id(section_id);

	    	request.setAttribute("editedUserData", editedUserData);

	        request.getRequestDispatcher("edit.jsp").forward(request, response);
        }

	}

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	int editedUserData_id = Integer.parseInt(request.getParameter("editedUserData_id"));
    	String login_id = request.getParameter("login_id");
    	String password = request.getParameter("password");
        String checkpassword = request.getParameter("checkpassword");
    	String original_login_id = request.getParameter("original_login_id");
        String user_name = request.getParameter("user_name");
        int branch_id = Integer.parseInt(request.getParameter("branch_id"));
        int section_id = Integer.parseInt(request.getParameter("section_id"));
System.out.println("b:" + branch_id);
System.out.println("s:" + section_id);

//System.out.println("validはいるまえ");


    	if (StringUtils.isBlank(login_id)) {
            messages.add("ログインIDを入力してください");
        } if (login_id != original_login_id){//もともとのログインId=original_login_id
	    	UserService userService = new UserService();
	        User existedUser = userService.checkNewUserId(login_id);
	        if (existedUser != null) {
	            if (existedUser.getId() != editedUserData_id) {
	            	messages.add("すでに使用されているログインIDです");
	            }
	        }
	    if(!login_id.matches("^\\w{6,20}$" )){
        	messages.add("ログインIDは半角英数字6～20字で入力してください");
            }
        }
        if (!StringUtils.isBlank(password)) {
        	if (!password.matches("^[\\-+*@?><,./_;:`!#$%&'()=~^|a-zA-Z0-9]{6,20}$")){
	        	messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
	        }
	        if (StringUtils.isBlank(checkpassword)) {
	            messages.add("確認用パスワードを入力してください");
	        }
        }
        if (password.matches(checkpassword) == false) {
            messages.add("パスワードが一致しません");
        }
        if (StringUtils.isBlank(user_name) == true) {
            messages.add("ユーザー名を入力してください");
        }
        if (user_name.length() > 10){
        	messages.add("ユーザー名は10文字以下で入力してください");
        }
        if (branch_id == 1){
            if (section_id != 1 && section_id != 2){
            	messages.add("支店と部署の組み合わせが不適切です");
            	System.out.println("e1");
            }
        }

        if (branch_id != 1){
        	if(section_id == 1 || section_id == 2){
        		messages.add("支店と部署の組み合わせが不適切です");
            	System.out.println("e1");
        	}
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
