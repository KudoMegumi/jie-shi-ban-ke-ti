package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Section;
import beans.User;
import service.BranchService;
import service.SectionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	System.out.println("get1");
    	List<Branch> branches = new ArrayList<Branch>();
    	BranchService branchService = new BranchService();

        List<Section> sections = new ArrayList<Section>();
    	SectionService sectionService = new SectionService();

    	branches = branchService.getAllBranches();
    	sections = sectionService.getAllSections();;

    	HttpSession session = request.getSession();
    	session.setAttribute("branches", branches);
    	session.setAttribute("sections", sections);
        request.getRequestDispatcher("signup.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	System.out.println("post1");
        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();

        if (isValid(request, messages) == true) {
System.out.println("post2");
            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setUser_name(request.getParameter("user_name"));
            user.setBranch_id(Integer.valueOf(request.getParameter("branch_id")));
            user.setSection_id(Integer.valueOf(request.getParameter("section_id")));
//            user.setIs_stopped(request.getParameter("is_stopped"));
System.out.println("post3");
            new UserService().register(user);
            System.out.println("post4");


            response.sendRedirect("management");

        } else {

            session.setAttribute("errorMessages", messages);

            //登録できなかった際に入力したワードを入力したままにする
            User notRegisterData = new User();
	    	String login_id = request.getParameter("login_id");
	    	String password = request.getParameter("password");
	    	String user_name = request.getParameter("user_name");
	    	int branch_id = Integer.parseInt(request.getParameter("branch_id"));
	    	int section_id = Integer.parseInt(request.getParameter("section_id"));

	    	notRegisterData.setLogin_id(login_id);
	    	notRegisterData.setPassword(password);
	    	notRegisterData.setUser_name(user_name);
	    	notRegisterData.setBranch_id(branch_id);
	    	notRegisterData.setSection_id(section_id);

	    	request.setAttribute("notRegisterData", notRegisterData);

            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String checkpassword = request.getParameter("checkpassword");
        String user_name = request.getParameter("user_name");
        int branch_id = Integer.parseInt(request.getParameter("branch_id"));
        int section_id = Integer.parseInt(request.getParameter("section_id"));


        if (StringUtils.isBlank(login_id)) {
            messages.add("ログインIDを入力してください");
        }
        if (!StringUtils.isBlank(login_id)) {
        	UserService userService = new UserService();
        	User existedUser = userService.checkNewUserId(login_id);
        	if (existedUser != null) {
        		messages.add("すでに使用されているログインIDです");
        	}
        	if(!login_id.matches("^\\w{6,20}$" )){
            	messages.add("ログインIDは半角英数字6～20字で入力してください");
            }
        }

        if (StringUtils.isBlank(password)) {
            messages.add("パスワードを入力してください");
        }
        if (!StringUtils.isBlank(password)) {
        	if (!password.matches("^[\\-+*@?><,./_;:`!#$%&'()=~^|a-zA-Z0-9]{6,20}$")){
        		messages.add("パスワードは半角文字6～20字で入力してください");
	        }
        }
        if (StringUtils.isBlank(checkpassword) == true) {
            messages.add("確認用パスワードを入力してください");
        }

        if (StringUtils.isBlank(checkpassword) == false) {
        	if (password.matches(checkpassword) == false) {
                messages.add("パスワードが一致しません");
            }
        }

        if (StringUtils.isBlank(user_name) == true) {
            messages.add("ユーザー名を入力してください");
        }
        if (user_name.length() > 10){
        	messages.add("ユーザー名は10字以下で入力してください");
        }
        if (branch_id == -1) {
        	if (section_id == -1) {//部署も支店も空白
        		messages.add("支店名を選択してください");
                messages.add("部署名を選択してください");
            } else {//支店は空白　部署はあり
            	messages.add("支店名を選択してください");
            }
        }
        if (branch_id == 1){
        	if (section_id == -1){
        		messages.add("部署名を選択してください");
        	} else if (section_id != 1 && section_id != 2) {
        		messages.add("支店と部署の組み合わせが不適切です");
        	}
        }
        if (branch_id != -1 && branch_id != 1) {
        	System.out.println(branch_id);
        	System.out.println(section_id);
        	if (section_id == -1){
        		messages.add("部署名を選択してください");
        	}
        	if ((section_id == 1) || (section_id == 2)) {
        		messages.add("支店と部署の組み合わせが不適切です");
        	}
        }


        /* else {
        	if (branch_id == 1 && !( section_id == 1 || section_id == 2)){
            	messages.add("支店と部署の組み合わせが不適切です3");
            }
            if (branch_id != 1 && ( section_id == 1 || section_id == 2)){
            	messages.add("支店と部署の組み合わせが不適切です2");
            }
        }
*/
        /*} else {
        	if (branch_id == 1 && !( section_id == 1 || section_id == 2)){
            	messages.add("支店と部署の組み合わせが不適切です3");
            }
            if (branch_id != 1 && ( section_id == 1 || section_id == 2)){
            	messages.add("支店と部署の組み合わせが不適切です4");
            }
        }*/


        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}