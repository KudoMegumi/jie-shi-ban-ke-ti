package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.DeleteCommentService;

/**
 * Servlet implementation class DeleteCommentServlet
 */
@WebServlet("/deleteComment")
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int messageId = Integer.parseInt(request.getParameter("message_id"));
		int commentId = Integer.parseInt(request.getParameter("comment_id"));
	   	DeleteCommentService deleteCommentService = new DeleteCommentService();
	    deleteCommentService.deleteComment(commentId);

//	    int messageId = Integer.parseInt(request.getParameter("message_id"));

	    List<String> messages = new ArrayList<String>();
    	messages.add("コメントを削除しました");

	    HttpSession session = request.getSession();
	    session.setAttribute("messageId", messageId);
	    session.setAttribute("commentDeleteMessages", messages);
	    response.sendRedirect("./?#delete"+messageId);

	}
}
