package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	String search_word = request.getParameter("search_word");
    	String start_date = request.getParameter("start_date");
    	if (start_date != null && !StringUtils.isEmpty(start_date)) {
    		request.setAttribute("start_date", start_date);
    	}
    	String end_date = request.getParameter("end_date");
    	if (end_date != null && !StringUtils.isEmpty(end_date)) {
    		request.setAttribute("end_date", end_date);
    	}
    	List<UserMessage> messages = new MessageService().getUserMessage(search_word,start_date,end_date);
    	List<Comment> comments = new CommentService().getComment();


    	int messages_size = messages.size();
//    	System.out.println(messages);
    	request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("search_word",search_word);
        request.setAttribute("messages_size", messages_size);


        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> messages = new ArrayList<String>();
   	 	HttpSession session = request.getSession();
   	 	System.out.println(Integer.parseInt(request.getParameter("message_id")));
   	 	int messageId = Integer.parseInt(request.getParameter("message_id"));

        if (isValid(request, messages) == true) {
System.out.println("いけてる");
            User user = (User) session.getAttribute("loginUser");
            int contribution_id = Integer.parseInt(request.getParameter("message_id"));

            Comment comment = new Comment();
            comment.setContribution_id(contribution_id);
            comment.setText(request.getParameter("comment_box"));
            comment.setUser_id(user.getId());

            System.out.println("comment：" + comment);

            new CommentService().register(comment);

            response.sendRedirect("./?#post"+messageId);
        } else {


//            String not_send_comment = request.getParameter("comment-box");
            String not_send_id = request.getParameter("message_id");
            String text = request.getParameter("comment_box");


            session.setAttribute("not_send_comment", text);
            session.setAttribute("not_send_id", not_send_id);
            session.setAttribute("commentErrorMessages", messages);
            response.sendRedirect("./?#comment"+messageId);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String text = request.getParameter("comment_box");

        if (StringUtils.isBlank(text) == true) {
            messages.add("コメントを入力してください");
        }
        if (500 < text.length()) {
            messages.add("500文字以下で入力してください");
        }

        System.out.println(messages);
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
