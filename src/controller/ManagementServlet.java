package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Branch;
import beans.Section;
import beans.User;
import service.BranchService;
import service.ManagementService;
import service.SectionService;

/**
 * Servlet implementation class ManagementServlet
 */
@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<User> userList = new ManagementService().getAllUsers();
        List<Branch> branchList = new BranchService().getAllBranches();
        List<Section> sectionList = new SectionService().getAllSections();

        request.setAttribute("userList",userList);
        request.setAttribute("branchList",branchList);
        request.setAttribute("sectionList",sectionList);
        request.getRequestDispatcher("management.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

    }
}
