package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserStateService;

/**
 * Servlet implementation class UserStateServlet
 */
@WebServlet(urlPatterns = { "/UserState" })
public class UserStateServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    }

    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

    	User user = new User();
    	int id = Integer.parseInt(request.getParameter("id"));
    	int is_stopped = Integer.parseInt(request.getParameter("is_stopped"));
    	user.setId(id);
    	user.setIs_stopped(is_stopped);


    	UserStateService userStateService = new UserStateService();
    	userStateService.updateUserState(user);

        response.sendRedirect("management");

	}
}
