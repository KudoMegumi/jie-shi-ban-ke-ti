package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/management","/edit","/signup"})
public class EditFilter implements Filter {
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

//			System.out.println("総務部チェック");

			HttpSession session = ((HttpServletRequest)request).getSession();
			List<String> message = new ArrayList<String>();

			User user = (User) session.getAttribute("loginUser");

			if(user == null){
				chain.doFilter(request, response);
				return;
			}
			//userがnullだったらすぐchainにいく→return;

			int branch_id = user.getBranch_id();
			int section_id = user.getSection_id();

			if(branch_id != 1 || section_id != 1){
				message.add("アクセスできません");
				session.setAttribute("errorMessages",message);
				((HttpServletResponse)response).sendRedirect("./");
			}else{
				chain.doFilter(request, response); // サーブレットを実行
			}
	}
	public void init(FilterConfig config) {
	}

	public void destroy() {
	}
}

