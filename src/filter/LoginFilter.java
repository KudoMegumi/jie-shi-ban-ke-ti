package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {
		public void doFilter(ServletRequest request, ServletResponse response,
				FilterChain chain) throws IOException, ServletException {

//			System.out.println("ログインチェック");

			String path = ((HttpServletRequest)request).getServletPath();

			HttpSession session = ((HttpServletRequest)request).getSession();
			List<String> message = new ArrayList<String>();

//System.out.println(path);
			if(!("/login".equals(path)) && !("/css/style.css".equals(path))){
				if(session == null){
					message.add("ログインしてください");
					((HttpServletResponse)response).sendRedirect("login");
				}
				User user = (User)session.getAttribute("loginUser");
				if(user == null){
					message.add("ログインしてください");
					session.setAttribute("errorMessages",message);
					((HttpServletResponse)response).sendRedirect("login");
				}else{
				chain.doFilter(request, response); // サーブレットを実行
				}
			}else{
			chain.doFilter(request, response);
			}
		}

		@Override
		public void init(FilterConfig config) {
		}

		@Override
		public void destroy() {
		}
	}

