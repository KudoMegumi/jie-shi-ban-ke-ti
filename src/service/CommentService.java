package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import dao.CommentDao;
import utils.CloseableUtil;

public class CommentService {
	public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public List<Comment> getComment() {

		List<Comment> comments = new ArrayList<Comment>();

		Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            comments = commentDao.getComment(connection);

            return comments;
        }finally{
        	CloseableUtil.close(connection);
        }

	}
}
