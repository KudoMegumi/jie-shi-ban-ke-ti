package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import dao.BranchDao;


public class BranchService {

	public List<Branch> getAllBranches() {

		List<Branch> branchList = new ArrayList<Branch>();

        Connection connection = null;
        try {

            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            branchList = branchDao.getAllBranches(connection);

            commit(connection);

            return branchList ;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}