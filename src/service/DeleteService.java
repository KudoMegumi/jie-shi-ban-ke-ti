package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.MessageDao;

public class DeleteService {
	public void deleteMessage(int contributionId){

		Connection connection = null;
		try{
			connection = getConnection();

			MessageDao deletedMessageDao = new MessageDao();
			deletedMessageDao.deleteMessage(connection,contributionId);

			commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
		}
	}
}
