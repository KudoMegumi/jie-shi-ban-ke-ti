package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.CommentDao;

public class DeleteCommentService {
	public void deleteComment(int commentId){

		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao deletedCommentDao = new CommentDao();
			deletedCommentDao.deleteComment(connection,commentId);

			commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
		}
	}
}
