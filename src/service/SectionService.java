package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import beans.Section;
import dao.SectionDao;

public class SectionService {
	public List<Section> getSectionData() {

		List<Section> sections = new ArrayList<Section>();

        Connection connection = null;
        try {

            connection = getConnection();

            SectionDao sectionDao = new SectionDao();
            sections = sectionDao.getSectionData(connection);

            commit(connection);

            return sections ;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
	public List<Section> getAllSections() {

		List<Section> sectionList = new ArrayList<Section>();

        Connection connection = null;
        try {

            connection = getConnection();

            SectionDao sectionDao = new SectionDao();
            sectionList = sectionDao.getAllSections(connection);

            commit(connection);

            return sectionList ;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}



}
