package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import dao.MessageDao;
import utils.CloseableUtil;

public class MessageService {

	public List<UserMessage> getUserMessage(String search_word, String start_date, String end_date){

		List<UserMessage> messages = new ArrayList<UserMessage>();

		Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messages = messageDao.getUserMessage(connection,search_word,start_date,end_date);

            return messages;
        }finally{
        	CloseableUtil.close(connection);
        }

	}

    public void register(UserMessage message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}