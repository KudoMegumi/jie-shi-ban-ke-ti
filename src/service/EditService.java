package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.EditedUserDao;
import dao.UserDao;
import utils.CipherUtil;

public class EditService {

	public void updateUserData(User user){

		Connection connection = null;
		try{
			connection = getConnection();

			if(!StringUtils.isEmpty(user.getPassword())){
				String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();
			userDao.updateEditedUser(connection,user);

			commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
		}
	}

 public User getEditedUser(int id) {

	    Connection connection = null;
	    try {

	    	User editedUserData = new User();
	        connection = getConnection();

	        EditedUserDao editedUserDao = new EditedUserDao();
	        editedUserData = editedUserDao.getEditedUser(connection, id);

	        commit(connection);

	        return editedUserData;

	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}
