package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import dao.UserDao;
public class ManagementService {

	public List<User> getAllUsers(){

		List<User> userList = new ArrayList<User>();

		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			userList = userDao.getAllUsers(connection);

			commit(connection);
			return userList;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
		}
	}
}
