package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String title;
    private String category;
    private int user_id;
    private String text;
    private Timestamp created_date;
    private String name;

	/**
	 * idを取得します。
	 * @return id
	 */
	public int getId() {
	    return id;
	}
	/**
	 * idを設定します。
	 * @param id id
	 */
	public void setId(int id) {
	    this.id = id;
	}
	/**
	 * titleを取得します。
	 * @return title
	 */
	public String getTitle() {
	    return title;
	}
	/**
	 * titleを設定します。
	 * @param title title
	 */
	public void setTitle(String title) {
	    this.title = title;
	}
	/**
	 * categoryを取得します。
	 * @return category
	 */
	public String getCategory() {
	    return category;
	}
	/**
	 * categoryを設定します。
	 * @param category category
	 */
	public void setCategory(String category) {
	    this.category = category;
	}
	/**
	 * user_idを取得します。
	 * @return user_id
	 */
	public int getUser_id() {
	    return user_id;
	}
	/**
	 * user_idを設定します。
	 * @param user_id user_id
	 */
	public void setUser_id(int user_id) {
	    this.user_id = user_id;
	}
	/**
	 * textを取得します。
	 * @return text
	 */
	public String getText() {
	    return text;
	}
	/**
	 * textを設定します。
	 * @param text text
	 */
	public void setText(String text) {
	    this.text = text;
	}
	/**
	 * created_dateを取得します。
	 * @return created_date
	 */
	public Timestamp getCreated_date() {
	    return created_date;
	}
	/**
	 * created_dateを設定します。
	 * @param created_date created_date
	 */
	public void setCreated_date(Timestamp created_date) {
	    this.created_date = created_date;
	}
	/**
	 * nameを取得します。
	 * @return name
	 */
	public String getName() {
	    return name;
	}
	/**
	 * nameを設定します。
	 * @param name name
	 */
	public void setName(String name) {
	    this.name = name;
	}
}