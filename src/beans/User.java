package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_id;
    private String password;
    private String encPassword;
    private String user_name;
    private int branch_id;
    private String branch_name;
    private int section_id;
    private String section_name;
    private int is_stopped;
    private Date createdDate;
    private Date updatedDate;
	/**
	 * idを取得します。
	 * @return id
	 */
	public int getId() {
	    return id;
	}
	/**
	 * idを設定します。
	 * @param id id
	 */
	public void setId(int id) {
	    this.id = id;
	}
	/**
	 * login_idを取得します。
	 * @return login_id
	 */
	public String getLogin_id() {
	    return login_id;
	}
	/**
	 * login_idを設定します。
	 * @param login_id login_id
	 */
	public void setLogin_id(String login_id) {
	    this.login_id = login_id;
	}
	/**
	 * passwordを取得します。
	 * @return password
	 */
	public String getPassword() {
	    return password;
	}
	/**
	 * passwordを設定します。
	 * @param password password
	 */
	public void setPassword(String password) {
	    this.password = password;
	}
	/**
	 * encPasswordを取得します。
	 * @return encPassword
	 */
	public String getEncPassword() {
	    return encPassword;
	}
	/**
	 * encPasswordを設定します。
	 * @param encPassword encPassword
	 */
	public void setEncPassword(String encPassword) {
	    this.encPassword = encPassword;
	}
	/**
	 * user_nameを取得します。
	 * @return user_name
	 */
	public String getUser_name() {
	    return user_name;
	}
	/**
	 * user_nameを設定します。
	 * @param user_name user_name
	 */
	public void setUser_name(String user_name) {
	    this.user_name = user_name;
	}
	/**
	 * branch_idを取得します。
	 * @return branch_id
	 */
	public int getBranch_id() {
	    return branch_id;
	}
	/**
	 * branch_idを設定します。
	 * @param branch_id branch_id
	 */
	public void setBranch_id(int branch_id) {
	    this.branch_id = branch_id;
	}
	/**
	 * branch_nameを取得します。
	 * @return branch_name
	 */
	public String getBranch_name() {
	    return branch_name;
	}
	/**
	 * branch_nameを設定します。
	 * @param branch_name branch_name
	 */
	public void setBranch_name(String branch_name) {
	    this.branch_name = branch_name;
	}
	/**
	 * section_idを取得します。
	 * @return section_id
	 */
	public int getSection_id() {
	    return section_id;
	}
	/**
	 * section_idを設定します。
	 * @param section_id section_id
	 */
	public void setSection_id(int section_id) {
	    this.section_id = section_id;
	}
	/**
	 * section_nameを取得します。
	 * @return section_name
	 */
	public String getSection_name() {
	    return section_name;
	}
	/**
	 * section_nameを設定します。
	 * @param section_name section_name
	 */
	public void setSection_name(String section_name) {
	    this.section_name = section_name;
	}
	/**
	 * is_stoppedを取得します。
	 * @return is_stopped
	 */
	public int getIs_stopped() {
	    return is_stopped;
	}
	/**
	 * is_stoppedを設定します。
	 * @param is_stopped is_stopped
	 */
	public void setIs_stopped(int is_stopped) {
	    this.is_stopped = is_stopped;
	}
	/**
	 * createdDateを取得します。
	 * @return createdDate
	 */
	public Date getCreatedDate() {
	    return createdDate;
	}
	/**
	 * createdDateを設定します。
	 * @param createdDate createdDate
	 */
	public void setCreatedDate(Date createdDate) {
	    this.createdDate = createdDate;
	}
	/**
	 * updatedDateを取得します。
	 * @return updatedDate
	 */
	public Date getUpdatedDate() {
	    return updatedDate;
	}
	/**
	 * updatedDateを設定します。
	 * @param updatedDate updatedDate
	 */
	public void setUpdatedDate(Date updatedDate) {
	    this.updatedDate = updatedDate;
	}
}
