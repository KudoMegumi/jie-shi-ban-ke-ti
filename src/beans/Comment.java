package beans;

import java.sql.Timestamp;

public class Comment {
	private int id;
	private String text;
	private int user_id;
	private int contribution_id;
    private Timestamp created_date;
    private String name;
	/**
	 * idを取得します。
	 * @return id
	 */
	public int getId() {
	    return id;
	}
	/**
	 * idを設定します。
	 * @param id id
	 */
	public void setId(int id) {
	    this.id = id;
	}
	/**
	 * textを取得します。
	 * @return text
	 */
	public String getText() {
	    return text;
	}
	/**
	 * textを設定します。
	 * @param text text
	 */
	public void setText(String text) {
	    this.text = text;
	}
	/**
	 * user_idを取得します。
	 * @return user_id
	 */
	public int getUser_id() {
	    return user_id;
	}
	/**
	 * user_idを設定します。
	 * @param user_id user_id
	 */
	public void setUser_id(int user_id) {
	    this.user_id = user_id;
	}
	/**
	 * contribution_idを取得します。
	 * @return contribution_id
	 */
	public int getContribution_id() {
	    return contribution_id;
	}
	/**
	 * contribution_idを設定します。
	 * @param contribution_id contribution_id
	 */
	public void setContribution_id(int contribution_id) {
	    this.contribution_id = contribution_id;
	}
	/**
	 * created_dateを取得します。
	 * @return created_date
	 */
	public Timestamp getCreated_date() {
	    return created_date;
	}
	/**
	 * created_dateを設定します。
	 * @param created_date created_date
	 */
	public void setCreated_date(Timestamp created_date) {
	    this.created_date = created_date;
	}
	/**
	 * nameを取得します。
	 * @return name
	 */
	public String getName() {
	    return name;
	}
	/**
	 * nameを設定します。
	 * @param name name
	 */
	public void setName(String name) {
	    this.name = name;
	}
}
