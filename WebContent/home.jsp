<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<title>ホーム</title>
<script>
<!--
	function disp(status) {
	var ans = window.confirm('削除してもよろしいですか？');
		if (ans) {
			return true;
		} else {
			return false;
		}
	}
//-->
</script>
</head>
<body>
	<div class="newHeader">BBS/ホーム<i class="fas fa-home"></i><p id="welcome">
<c:forEach items="${branchList}" var="branch">
							<c:if test="${loginUser.branch_id == branch.id}">
									<c:out value="${branch.name}" />
							</c:if>
						</c:forEach>

<c:forEach items="${sectionList}" var="section">
							<c:if test="${loginUser.section_id == section.id}">
									<c:out value="${section.name}" />
							</c:if>
						</c:forEach>：
<c:out value="${loginUser.user_name}" /> さん</p></div>
	<div id="home_left_area">
		<div class="link_top">
			<a href="newMessage" class="link_square_btn">新規投稿</a></div>
				<div class="link">
					<c:if test="${loginUser.branch_id == 1 && loginUser.section_id == 1}">
						<a href="management" class="link_square_btn">ユーザー管理</a>
					</c:if>
				</div>
			<div class="link"><a href="logout" class="link_square_btn">ログアウト</a></div>
		<div class="search">
		<div class="mini_header">
		<div class="search_word"><i class="fas fa-search fa-fw my-small"></i>投稿検索</div></div><!-- 検索窓 -->
			<div class="input_field">
				<form action="./" method="get">
					<label for="search_word">カテゴリ検索
						<input name="search_word" id="search_word" value="${search_word}" size="15" placeholder="キーワードを入力"/>
					</label><br />
					日時検索
					<div class="search_date">
					<label for="start_date">
					<input type="date" name="start_date" id="start_date" value="${start_date}" size="15" />から</label>
					<label for="end_date">
					<input type="date" name="end_date" id="end_date" value="${end_date}" size="15" />まで
					</label></div>
					<input type="submit" value="検索" class="square_btn">
					<a href="./" class="square_btn">リセット</a>
				</form>
			</div>
		</div>
	</div>
	<br />

	<div id="home_right_area">
	<div class="home_errorMessages">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" /><br/>
				</c:forEach>
		</div>
	<c:remove var="errorMessages" scope="session" />

	<div class="number_of_messages">
	<c:if test="${messages_size != 0}">
		投稿件数：<c:out value="${messages_size}"></c:out>件<br/>
	</c:if>
	</div>

		<!-- 投稿表示 -->

		<c:forEach items="${messages}" var="message">

		<div class="contribution" >
			<div class="contribution_title">
			<i class="far fa-clipboard fa-lg"></i>
				<c:out  value="${message.title}" />
			</div>
			<div class="text" >

				<c:forEach var="s" items="${fn:split(message.text,'
')}">
					<c:out value="${s}" />
					<br />
				</c:forEach>

			<div class="category">
				カテゴリ：
				<c:out value="${message.category}" />
			</div>
			<div class="user_name">
				投稿者　：
				<c:out value="${message.getName()}" />
			</div><br>
			<div class="date">
				投稿日時：
				<fmt:formatDate value="${message.created_date}"
					pattern="yyyy/MM/dd HH:mm:ss" />
			</div>


			<div class="delete">
				<!-- 投稿削除 -->
				<form action="delete" method="post">
					<input type="hidden" name="contribution_id" value="${message.id}">
					<c:if test="${message.user_id == loginUser.id }">
						<input type="submit" value="投稿削除" class="square_btn_r"  onClick="return disp(this.value);">

					</c:if>

				</form>
			</div>


			</div>


			<div class="comment">

			<div class="number_of_comments" id="post${message.id}">
<%--
				<c:if test="${comments_size != 0}"> --%>
					<i class="fas fa-comment-alt fa-fw"></i>コメント<%-- <c:out value="${comments_size}"></c:out>件<br/>
				</c:if> --%>
			</div>



				<!-- コメント表示 -->
				<c:forEach items="${comments}" var="comment">

					<c:if test="${comment.contribution_id == message.id }">
						<div class="comment_text">
							<c:forEach var="s" items="${fn:split(comment.text,'
')}">
								<c:out value="${s}" />
								<br />
							</c:forEach>
						</div>
						<div class="comment_user_name">
							コメント投稿者：<c:out value="${comment.getName()}" />
						</div>
						<div class="comment_date">
							コメント日時　：<fmt:formatDate value="${comment.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>

						<div class="deleteComment">
							<!-- コメント削除 -->
							<form action="deleteComment" method="post">
								<input type="hidden" name="message_id" value="${message.id}">
								<input type="hidden" name="comment_id" value="${comment.id}">
								<c:if test="${comment.user_id == loginUser.id }">
									<input type="submit" value="コメント削除" class="square_btn_r"  onClick="return disp(this.value);">
									<br />
									<br />
								</c:if>
							</form>
						</div>
					</c:if>
				</c:forEach>


				<div class="commentDeleteMessages" id="delete${message.id}">
				<c:if test="${messageId == message.id }">
				<c:forEach items="${commentDeleteMessages}" var="del_message">
					<c:out value="${del_message}" />
						<c:remove var="commentDeleteMessages" scope="session" />
						<c:remove var="messageId" scope="session" />
				</c:forEach></c:if>
		</div>




				<div class="input_comment" id="comment${message.id}">
			<!-- コメント入力欄 -->
			<form action="./" method="post">
				<input type="hidden" name="message_id" value="${message.id}">
				<textarea cols="50" rows="5"  class="comment-box" id="comment_box" name="comment_box" placeholder="500字以内"     resize: none;><c:if test="${not_send_id == message.id}">${not_send_comment}<c:remove var="not_send_comment" scope="session" /></c:if></textarea><br />
				<input type="submit" value="コメント投稿" class="square_btn" >
				<br />
			</form>
			</div>
			<div class="commentErrorMessages">
			<c:if test="${ not empty commentErrorMessages }">

					<c:forEach items="${commentErrorMessages}" var="commentErrorMessage">
						<c:if test="${not_send_id == message.id}">
							<c:out value="${commentErrorMessage}" /><br/><br/>
							<c:remove var="commentErrorMessages" scope="session" />
						</c:if>
					</c:forEach>
					</c:if>
		</div>
			</div>


		</div>


		</c:forEach>
	</div>
<div id="copyright">Copyright(c)Megumi Kudo</div>
</body>

</html>