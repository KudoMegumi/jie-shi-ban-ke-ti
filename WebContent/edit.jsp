<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
</head>
<body>
<div class="newHeader">BBS/ユーザー編集<i class="fas fa-edit fa-fw"></i><p id="welcome">
<c:forEach items="${branchList}" var="branch">
							<c:if test="${loginUser.branch_id == branch.id}">
									<c:out value="${branch.name}" />
							</c:if>
						</c:forEach>

<c:forEach items="${sectionList}" var="section">
							<c:if test="${loginUser.section_id == section.id}">
									<c:out value="${section.name}" />
							</c:if>
						</c:forEach>：
<c:out value="${loginUser.user_name}" /> さん</p></div>
<div id="onePage_left_area">
	<a href="management" class="link_square_btn">ユーザー管理</a>
</div>

<div id="onePage_right_area">
	<div class="edit_errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" /><br/>
				</c:forEach>
			</ul>
		</div>
			<c:remove var="errorMessages" scope="session" />
<div class="edit_input_form">
	<form action="edit" method="post">
	<div class ="input_login_id_form">
		<input type="hidden" name="editedUserData_id" value="${editedUserData.id}">
		<label for="login_id">ログインID　　　
		<p1><input type="hidden" name="original_login_id" value="${editedUserData.login_id}">
		<input name="login_id" value="${editedUserData.login_id}" id="login_id" placeholder="半角英数字6～20字" size="25"/></label>
		</p1>
	<div class = input_password_form>
		<label for="password">パスワード　　　　
		<input name="password" type="password" id="password" placeholder="半角文字6～20字" size="25" /></label>
	</div>
	<div class = input_checkpassword_form>
		<label for="checkpassword">確認用パスワード　</label>
		<input name="checkpassword" type="password" id="checkpassword" size="25"/> <br />
	</div>
	<div class = input_user_name_form>
		<label for="user_name">ユーザー名　　　　
		<input name="user_name" value="${editedUserData.user_name}" id="user_name" placeholder="10字以内" size="25"/></label>
	</div>
	<div class = input_branch_id_form>
		<c:if test="${editedUserData.id == loginUser.id }">
			<label for="branch_id">支店名　　　　　　</label>
				<c:forEach items="${branchList}" var="branch">
					<c:if test="${editedUserData.branch_id == branch.id}">
						<c:out value="${branch.name}" />
						<input type="hidden" name="branch_id" value="${branch.id }">
					</c:if>
				</c:forEach>

			<br />
			<label for="section_id">部署名　　　　　　</label>
				<c:forEach items="${sectionList}" var="section">
					<c:if test="${editedUserData.section_id == section.id}">
						<c:out value="${section.name}" />
						<input type="hidden" name="section_id" value="${section.id }">
					</c:if>
				</c:forEach>


		</c:if>
	</div>
	<div class = input_section_id_form>
		<c:if test="${editedUserData.id != loginUser.id }">
			<label for="branch_id">支店名　　　　　　</label>
			<select name="branch_id">
				<c:forEach items="${branchList}" var="branch">
					<option value="${branch.id }" ${(branch.id == editedUserData.branch_id) ? 'selected' : ''}>
						<c:out value="${branch.name}"></c:out></option>
				</c:forEach>
			</select>
			<br />
			<label for="section_id">部署名　　　　　　</label>
			<select name="section_id">
				<c:forEach items="${sectionList}" var="section">
					<option value="${section.id }" ${(section.id == editedUserData.section_id) ? 'selected' : ''} size="25">
						<c:out value="${section.name}"></c:out></option>
				</c:forEach>
			</select>
		</c:if>
		</div>
		<div class="sign_up_btn">
		<input type="submit" value="更新" class="square_btn"/></div>
	</form>
	</div>
	</div>
	</div>

<div id="onePage_copyright">Copyright(c)Megumi Kudo</div>
</body>
</html>