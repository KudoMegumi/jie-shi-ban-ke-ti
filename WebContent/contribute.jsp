<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<title>新規投稿</title>
</head>
<body>
<div class="newHeader">BBS/新規投稿<i class="fas fa-pencil-alt fa-fw"></i><p id="welcome"><c:out value="${loginUser.user_name}" /> さん</p></div>
<div id="onePage_left_area">
<div class="link">
	<a href="./" class="link_square_btn">ホーム</a>
</div>
</div>
<div id="onePage_right_area">
	<div class="contribute_errorMessages">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" /><br/>
				</c:forEach>
		</div>
			<c:remove var="errorMessages" scope="session" />
		<div class="newMessage_input_form">
	<form action="newMessage" method="post">
		<input type="hidden" name="id" value="${missedContribution.id}">
		<label for="title">件名　　　
		<input name="title" id="title" value="${missedContribution.title}" placeholder="30字以内" size="25" /></label><br/>
		<label for="category">カテゴリ　
		<input name="category" id="category" value="${missedContribution.category}" placeholder="10字以内" size="25" /></label><br/>
		<label for="text">本文<br/>
		<textarea cols="80" rows="8" class="contribution-box" id="text" name="text" placeholder="1000字以内">${missedContribution.text}</textarea></label>
		<br />
		<div class="contribute_btn">
		<input type="submit" value="投稿" class="square_btn"/></div>
	</form>
	</div></div>
<div id="onePage_copyright">Copyright(c)Megumi Kudo</div>
</body>
</html>
