<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>

</head>
<body>
<body id="login">
	<div class="newHeader">BBS/ログイン<i class="fas fa-sign-in-alt fa-fw"></i></div>

<div class="login_form">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />

	<form action="login" method="post">
	<div class="login_form_1">
			<div class = login_login_id_form>
				<label for="login_id">ログインID　
				<p2><input name="login_id" id="login_id" value="${missed_login_id }"/></label></p2><br>
			</div>
			<div class = login_password_form>
				<label for="password">パスワード　
				<input name="password" type="password" id="password"/></label><br/>
			</div>
		</div>

		<input type="submit" value="ログイン" class="square_btn"><br />
		</form>
		</div>
		<br />
	<div id="onePage_copyright">Copyright(c)Megumi Kudo</div>
	</body>
</html>