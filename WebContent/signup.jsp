<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
</head>
<body>
<div class="newHeader">BBS/ユーザー新規登録 <i class="fas fa-user-plus fa-fw"></i><p id="welcome">
<c:forEach items="${branchList}" var="branch">
							<c:if test="${loginUser.branch_id == branch.id}">
									<c:out value="${branch.name}" />
							</c:if>
						</c:forEach>

<c:forEach items="${sectionList}" var="section">
							<c:if test="${loginUser.section_id == section.id}">
									<c:out value="${section.name}" />
							</c:if>
						</c:forEach>：
<c:out value="${loginUser.user_name}" /> さん</p></div>
<div id="onePage_left_area">
<div class ="link">
	<a href="management" class="link_square_btn">ユーザー管理</a></div>
</div>

<div id="onePage_right_area">
		<div class="sign_up_errorMessages">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" /><br/>
				</c:forEach>
		</div>
			<c:remove var="errorMessages" scope="session" />

			<!-- <i class="far fa-user my-big"></i> -->
<div class="sign_up_input_form">
			<form action="signup" method="post">

			<div class ="input_login_id_form">
				<label for="login_id">ログインID　　　
				<p1><input name="login_id" value="${notRegisterData.login_id}" id="login_id" placeholder="半角英数字6～20字" size="25"/></label>
			</p1>
			<div class = input_password_form>
				<label for="password">パスワード　　　　
				<input name="password" type="password" id="password" placeholder="半角文字6～20字" size="25"/></label>
			</div>
			<div class = input_checkpassword_form>
				<label for="checkpassword">確認用パスワード　</label>
				<input name="checkpassword" type="password" id="checkpassword" size="25"/>
			</div>
			<div class = input_user_name_form>
				<label for="user_name">ユーザー名　　　　
				<input name="user_name" value="${notRegisterData.user_name}" id="user_name" placeholder="10字以内" size="25"/></label>
			</div>
			<div class = input_branch_id_form>
				<label for="branch_id">支店名　　　　　　</label>
				<select name="branch_id">
					<option value="-1">支店名を選択してください</option>
						<c:forEach items="${branches}" var="branch">
							<option value="${branch.id }" ${(branch.id == notRegisterData.branch_id) ? 'selected' : ''}>
								<c:out value="${branch.name}"></c:out>
							</option>
						</c:forEach>
				</select>
			</div>
			<div class = input_section_id_form>
				<label for="section_id">部署名　　　　　　</label>
				<select name="section_id">
					<option value="-1" size="30">部署名を選択してください</option>
					<c:forEach items="${sections}" var="section">
						<option value="${section.id }" ${(section.id == notRegisterData.section_id) ? 'selected' : ''}>
							<c:out value="${section.name}"></c:out>
						</option>
					</c:forEach>
				</select>
			</div>
		</div>
<div class="sign_up_btn">
<input type="submit" value="登録" class="square_btn"></div>
			</form>
			</div>		<div id="onePage_copyright">Copyright(c)Megumi Kudo</div>
</body>

</html>