<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<title>ユーザー管理</title>
<script>
<!--
	function disp(status) {
		var ans = window.confirm(status + 'させますか？');
		if (ans) {
			return true;
		} else {
			return false;
		}
	}
//-->
</script>
</head>
<body>
<div class="newHeader">BBS/ユーザー管理<i class="fas fa-folder-open fa-fw"></i>
<p id="welcome">
<c:forEach items="${branchList}" var="branch">
							<c:if test="${loginUser.branch_id == branch.id}">
									<c:out value="${branch.name}" />
							</c:if>
						</c:forEach>

<c:forEach items="${sectionList}" var="section">
							<c:if test="${loginUser.section_id == section.id}">
									<c:out value="${section.name}" />
							</c:if>
						</c:forEach>：
<c:out value="${loginUser.user_name}" /> さん</p></div>
	<div id="home_left_area">
	<div class="link">
		<a href="signup" class="link_square_btn">ユーザー新規登録</a></div>

	<div class="link">
		<a href="./" class="link_square_btn">ホーム</a>
	</div></div>
<div id="management_right_area">
	<div class="management_errorMessages">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" />
				</c:forEach>
		</div>
	<c:remove var="errorMessages" scope="session" />

	<div class="main-contents">
		<table>

			<tr class="management_header">
				<th>ユーザー名</th>
				<th>　ログインID　</th>
				<th>支店名</th>
				<th>部署名</th>
				<th>状態</th>
				<th>停止/復活</th>
				<th>編集</th>
			</tr>


			<c:forEach items="${userList}" var="user">

				<tr>
					<td><span class="title"><c:out
								value="${user.user_name}" /></span></td>
					<td><span class="title"><c:out value="${user.login_id}" /></span></td>

					<td><c:forEach items="${branchList}" var="branch">
							<span class="title">
							<c:if test="${user.branch_id == branch.id}">
									<c:out value="${branch.name}" />
								</c:if></span>
						</c:forEach></td>

					<td><c:forEach items="${sectionList}" var="section">
							<span class="title"> <c:if
									test="${user.section_id == section.id}">
									<c:out value="${section.name}" />
								</c:if></span>
						</c:forEach></td>

					<td><c:if test="${user.is_stopped == 0}">
							<c:out value="　稼働　"></c:out></c:if>
						<c:if test="${user.is_stopped == 1}">
							<c:out value="　停止　"></c:out>
						</c:if></td>

					<td><c:if test="${user.id == loginUser.id }">
							<span class="title">ログイン中</span>
						</c:if> <c:if test="${user.is_stopped == 0 && user.id != loginUser.id}">
							<form action="UserState" method="post">
								<input type="hidden" name="id" class="id" value="${user.id}">
								<span class="title"><input type="submit" value="停止" class="m_square_btn" onClick="return disp(this.value);" /></span>
									<input type="hidden" name="is_stopped" value="1">
							</form>
						</c:if> <c:if test="${user.is_stopped == 1 && user.id != loginUser.id}">
							<form action="UserState" method="post">
								<input type="hidden" name="id" value="${user.id}"> <span
									class="title"><input type="submit" value="稼動" class="m1_square_btn" onClick="return disp(this.value);" /></span> <input type="hidden"
									name="is_stopped" value="0">
							</form>
						</c:if></td>

					<td><form action="edit" method="get">
							<input type="hidden" name="id" value="${user.id}">
							<span class="title">
							<input type="submit" value="編集" class="m_square_btn"/></span>
						</form></td>
				</tr>
			</c:forEach>

		</table>
	</div>
	</div>
	<div id="copyright">Copyright(c)Megumi Kudo</div>
</body>
</html>