create table users(
	id          	INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	login_id		VARCHAR(20)		UNIQUE NOT NULL,
	password		VARCHAR(255)	NOT NULL,
	user_name		VARCHAR(20)		NOT NULL,
	branch_id		INTEGER,
	section_id		INTEGER,
	is_stopped		INTEGER,
	created_date 	TIMESTAMP      	NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    updated_date 	TIMESTAMP      	NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table branches(
	id          	INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	name			VARCHAR(10)		UNIQUE NOT NULL,
	created_date	TIMESTAMP      	NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    updated_date 	TIMESTAMP      	NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table sections(
	id          	INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	name			VARCHAR(10)		NOT NULL,
	created_date	TIMESTAMP      	NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    updated_date 	TIMESTAMP      	NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table contributions(
	id    		    INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	title			VARCHAR(30)		NOT NULL,
	text			text			NOT NULL,
	category		VARCHAR(10)		NOT NULL,
	created_date 	TIMESTAMP   	NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_id		    INTEGER			NOT NULL
);

create table comments(
	id    		    INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	contribution_id	INTEGER			NOT NULL,
	text			text			NOT NULL,
	created_date 	TIMESTAMP   	NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_id		    INTEGER			NOT NULL
);

